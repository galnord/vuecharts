// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import Axios from 'axios'
import VueRouter from 'vue-router'
//importamos el archivo de rutas siempre entre llaves
import { rutas } from './rutas.js'

Vue.config.productionTip = false

Vue.use(VueRouter);

export const bus = new Vue();

//cargamos el router con el archivo de rutas
const enrutador = new VueRouter({
  routes: rutas,
  mode: 'history'
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  components: { App },
  //le decimos a router de donde sacar las rutas
  router: enrutador,
  template: '<App/>'
})
