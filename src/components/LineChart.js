import { Line } from 'vue-chartjs'

export default Line.extend({
    mounted(){
        this.renderChart({
            labels: ['1hrs', '1día', '7 días'],
            datasets: [
                {label: 'Moneditas', backgroundColor: '#000', data: [40,39,31]}
            ]
        }, {responsive: true, maintainAspectRatio: false})
    }
})