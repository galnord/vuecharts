//se llama al componente que se va a utilizar en la ruta
import CurrencieList from './components/CurrencieList'
import CurrencieView from './components/CurrencieView'

export const rutas = [
    { path: '', component: CurrencieList },
    { path: '/currencie/:id', component: CurrencieView, name: 'coin' }
]